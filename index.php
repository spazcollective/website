<?php
	$bgNum=rand(0, 2);
	$bgURL;
	if ($bgNum==0){
		$bgURL = '/images/tvStatic/stat2.gif';
	}else if ($bgNum==1){
		$bgURL = '/images/tvStatic/stat3.gif';
	}else if ($bgNum==2){
		$bgURL = '/images/tvStatic/stat4.gif';
	}
/*
error_reporting(E_ALL);
ini_set(display_errors,1);
*/

?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<html>
  <head>
	<title>S.P.A.Z. Mutant Dashboard</title>
	<!-- removed maximum-scale=1 from viewport-->
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Semi-Permanent Autonomous Zone" />
	<meta name="keywords" content="spaz, soundsystem, free, tekno, techno, music, s.p.a.z., " />

	<link href="css/spazbeta5.css" rel="stylesheet" type="text/css" />
	<link href="skins/green.terbo/jplayer.green.terbo2.css" rel="stylesheet" type="text/css" />
	<link href="css/terbohome2.css" rel="stylesheet" type="text/css" />
	<link href="css/textiles.css" rel="stylesheet" type="text/css" />
	<link href="shadowbox/shadowbox.css" rel="stylesheet" type="text/css" />
	<link href="css/program-schedule.css" rel="stylesheet" type="text/css" /> <!-- removed from the plugin dirs -->

	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
	<script type="text/javascript" src="js/jquery.jplayer.min.js"></script>
	<script type="text/javascript" src="js/jqui.js"></script>
	<!-- jquery touch punch //touchpunch.furf.com/ .. makes jqUI's "draggable" into touch events on mobile -->
	<script type="text/javascript" src="js/jqtp.js"></script> 
	<script type="text/javascript" src="js/player.js"></script>
	<script type="text/javascript" src="js/tip.js"></script>
    <script type="text/javascript" src="js/playing.js"></script>
	<script type="text/javascript" src="js/schedule.js"></script>
	<script type="text/javascript" src="js/fullschedule.js"></script>
	<!-- have transfered below into this file, eventually all js goes together elsewhere -->
	<!--script type="text/javascript" src="js/cycling[mod].js"></script--> 
	<script type="text/javascript" src="shadowbox/shadowbox.js"></script>

	<!-- Make spaz logos show up on facebook, etc.  -->
	<link rel="image_src" href="/images/logo/logo_spaz_sunfront_24bit_300x.png" />
	<link rel="image_src" href="/images/logo/logo_spaz_3D_256_300x.png" />
	<link rel="image_src" href="/images/logo/logo_spaz_napkin_256_300x.png" />
	<link rel="image_src" href="/images/logo/logo_spaz_sunfront_256_300x.png" />
	<link rel="image_src" href="/images/logo/logo_spaz_BWball_256_300x.png" />
	<link rel="image_src" href="/images/logo/logo_spaz_patch_256_300x.png" />
	<link rel="image_src" href="/images/logo/logo_spaz_bitclouds_256_300x.png" />

	<style>
		body {
			//-webkit-perspective: 800px;
			//-moz-perspective: 800px;
			//perspective: 800px;
			overflow:hidden;
		}

		a:after{
			position: absolute;
			display: inline-block;
			width: 13px;
			height: 13px;
			content:' ';
		}

		a:hover:after{
			background: url('images/UIels/ExternalLinkIcon.gif');
		}

		/* no external link image for local links! need to fix this better */
		#archivelink:hover:after{
			background: url('');
		}
		/*
		.loggedIn, .loggedOut{
			display:none;
		}

		.state-loggedIn .loggedIn{
			display:block;
		}

		.state-loggedOut .loggedOut{
			display:block;
		}*/


		#workModeOverlay{
			background-color:#00ff00;
			width:100%;
			height:100%;
			position:fixed;
			top:0px;
			left:0px;
			display:none;
		}

		.spazti-mode-0 #workModeOverlay{
			display:block;
		}

		#logo, #back,#dirt,#masks,#pics{
			transition:all 60s;
			display:block;
			//-webkit-transform: rotateZ( 720deg ) rotateY( +720deg ) rotateX( -25deg ) translateZ(300px);
			//-webkit-transform: rotate(0deg) rotateZ( 2deg ) rotateY( +45deg ) rotateX( -25deg ) translateZ(100px);
		}

		/*.spazti-mode-2 div#logo.logo:hover,
		.spazti-mode-3 div#logo:hover,*/
		.spazti-mode-3 div#back:hover,
		.spazti-mode-3 div#dirt:hover,
		.spazti-mode-3 div#masks:hover,
		.spazti-mode-3 div#pics:hover, .hoverspin img:hover{
			-transform:scale(1.5,2) rotate(200000deg);
			-ms-transform:scale(1.5,2) rotate(200000deg);
			-moz-transform:scale(1.5,2) rotate(200000deg);
			-webkit-transform:scale(1.5,2) rotate(200000deg);
		}

		.logo {
			position:absolute;
			width: 400px;
			height:400px;
			margin: 0 auto;
			left:0;
			right:0;
		}

		#logo img {
			transition:transform 60s;
		}

		.hoverspin img:hover {
			-transform:scale(1.1,2) rotate(200000deg);
			-ms-transform:scale(1.1,2) rotate(200000deg);
			-moz-transform:scale(1.1,2) rotate(200000deg);
			-webkit-transform:scale(1.1,2) rotate(200000deg);
		}

		.spazti-mode-0 #back,.spazti-mode-0 #dirt,.spazti-mode-0 #masks{
			display:none;
		}

		#introSpaz{
			position: absolute;
			z-index: 127;
			left: 40%;
			width: 300px;
			top: -40%;
			text-align: center;
			-webkit-transform: rotate(45deg);
			-webkit-perspective: 400px;
			-moz-transform: rotate(45deg);
			-moz-perspective: 400px;
			transform: rotateY( 45deg );
		}

		#introSpaz h1{
			font-size:400px;
		}

		#introSpaz h2{
			font-size:50px;
		}

		#player {
			background: transparent;
			font-size:1.2em;
		}

		#orListen{
			-webkit-transform: rotate(88deg);
			-moz-transform: rotate(88deg);
			-transform: rotate(88deg);
			position: absolute;
			top: 123px;
			left: 140px;
			width: 199px;
			z-index:100;
		}

				.radio-clear{
			display:block;
		}

		#spaz_nowplaying{

		}

		#externalPlayLink{
			z-index:1;
			background-color:black;
			width:195px;
			padding:10px;
			border:1px solid green;
			position:relative;
			top:-20px;
			left:12px;
		}

		#jp_container_1{
			position: absolute;
			top: 250px;
			left: 142px;
			position:absolute;
		}

		#spastiPlane{
			//-webkit-transform:  rotateY( -5deg )  ;
			//transform:  rotateY( -5deg ) ;
			position:absolute;
			width:0px;
			z-index:100;
		}

		#spastiPlane2{
			//-webkit-transform: rotateZ( 0deg ) rotateY( +5deg ) rotateX( -5deg ) translateZ(100px);
			/* //transform: rotateZ( 0deg ) rotateY( +5deg ) rotateX( -5deg ); */
			position:absolute;
			width:0px;
			right:0px;
			z-index:100;
		}

		.widj{
			display:none;
			float:left;
			//margin:10px;
			z-index:100;
			width:250px;
			//min-width:250px;
			cursor:move;
			position: fixed;
			-webkit-overflow-scrolling: touch;
			-moz-border-radius: 52px;
			-webkit-border-radius: 52px;
			 border-radius: 52px;
			overflow:hidden;
			/*
			box-shadow: 0px 0px 50px 14px rgba(255, 200, 200, 1);
			-moz-box-shadow: 0px 0px 50px 14px rgba(0, 0, 0, 1);
			-webkit-box-shadow: 0px 0px 50px 0px rgba(20, 0, 0, 1);
			*/
		}

		.widj.overflowed{
			-moz-border-bottom-right-radius: 0px;
			-webkit-border-bottom-right-radius: 0px;
			 border-bottom-right-radius: 0px;
			-moz-border-top-right-radius: 0px;
			-webkit-border-top-right-radius: 0px;
			 border-top-right-radius: 0px;

		}



		.widj .content, .overlay .content{
			position:relative;
			width: 91%;
			height: 100%;
			left:0px;
			padding:10px;
			overflow-y:auto;
			overflow-x:hidden;
			-webkit-overflow-scrolling: touch;
			/*-moz-border-radius: 52px;
			-webkit-border-radius: 52px;
			 border-radius: 52px;*/
			/* above cuts off part of genre list on djSched */
		}

		.overlay .content {
			overflow-x:auto;
		}

		 .overflowed.widj .content{
			width:auto;
		 }

		.BGGlass{
			-moz-border-radius: 52px;
			-webkit-border-radius: 52px;
			border-radius: 52px;
			box-shadow: inset 0px 0px 100px 14px rgba(0, 0, 0, 1);
			-moz-box-shadow: inset 0px 0px 100px 14px rgba(0, 0, 0, 1);
			-webkit-box-shadow: inset 0px 0px 100px 14px rgba(0, 0, 0, 1);
			position:absolute;
			top:-5%;
			width:100%;
			height:110%;
			opacity:.8;
			background-color:black;
			background: url(<?php echo $bgURL ?>);
				/* bg url set on doc.ready */
		}

		/*.BGGlassGIF1 {
			background: url('images/tvStatic/stat2.gif');
		}

		.BGGlassGIF2 {
			background: url('images/tvStatic/stat3.gif');
		}

		.BGGlassGIF3 {
			background: url('images/tvStatic/stat4.gif');
		}*/

	


		.overflow  .BGGlass{
			-moz-border-bottom-right-radius: 0px;
			-webkit-border-bottom-right-radius: 0px;
			 border-bottom-right-radius: 0px;
			-moz-border-top-right-radius: 0px;
			-webkit-border-top-right-radius: 0px;
			 border-top-right-radius: 0px;


		}

		.spazti-mode-0 .BGGlass, .spazti-mode-1 .BGGlass{
			background:url('') #333 ;
			opacity:.9;
		}

		.spazti-mode-0 .BGGlass{
			background:url('') #333 ;
			opacity:1;
		}

		.overlay .BGGlass{
			background:url('') #333;
			opacity:.9;
		}

		.widj h1, .widj h2, {
			text-align:center;
		}

		.widj ul{
			list-style: none;
			padding:0px;
			margin:0px;
		}

		.ai1ec-date div{
			display:inline-block;
		}

		.overlay .ai1ec-event-title{
			font-size:22px;
		}

		.overlay .ai1ec-event{
			width:170px;
			height:300px;
			display:inline-block;
			vertical-align: top;
		}

		.overlay .ai1ec-event{
			overflow:hidden;
		}

		.ai1ec-content_img img{
			width:auto !important;
			max-width:300px !important;
			height:auto !important;
			max-height:300px !important;
		}


		.ai1ec-event img{
			max-width:200px;
			max-height:170px;
		}

		.ai1ec-event-location{
			font-size:12px;
			display:block;
		}

		.overlay{
			position:fixed;
			display:none;
			z-index:100001;
			left:5%;
			top:8%;
			bottom:8%;
			right:5%;
		}

		#overlayBG{
			display:none;
			position:absolute;
			width:100%;
			height:100%;
			z-index:100000;
			/*background:url('/images/tvStatic/stat4.gif');*/

		}


		.ui-resizable-handle{
			width:45px; height:45px;
			border:0px solid red;
			z-index:10;
			position:absolute;
		}

		.ui-resizable-se{
			z-index:11;
			right:0px;
			bottom:-10px;
			cursor:se-resize;
			background:url('//upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Bullseye1.png/45px-Bullseye1.png');
		}



		/* .hide,  */
		.ai1ec-event-description, .ai1ec-popup-excerpt, .wpuf-attachments, .XXentry-header, .widgettitle ,.ai1ec-allday-badge, .page .entry-title, .ai1ec-calendar-link, .ai1ec-date-title, .widj .ai1ec-event-title, .archive-title, .gallery-caption, .archive-meta, .btn-toolbar, .ai1ec-pagination,#ugc-media-form
		{
			display:none!important;
			visibility:hidden!important;
		}

		.ai1ec-day, .ai1ec-month, .ai1ec-weekday{
			display:none;
		}

		#mode-0-check, #mode-1-check, #mode-2-check, #mode-3-check{
			visibility:hidden;
		}

		.spazti-mode-0 #mode-0-check, .spazti-mode-1 #mode-1-check, .spazti-mode-2 #mode-2-check, .spazti-mode-3 #mode-3-check{
			visibility:visible;
		}


		.pGal .entry-title{
			display:block;
			visibility:visible;
		}



		.overlay .ai1ec-event{
			width:250px;
			padding:10px;
			margin:5px;
			background-color:black;
			background-color: rgb(255, 0, 235);
			box-shadow: 0px 0px 19px #000;
		}

		.ai1ec-event{

		}


		#loginFrame{
			border:0px;
		}


		#pGal .entry-title a:hover:after{
			background: url('');
			font-size:10px;
			content:'Upload to this gallery';
			display:block;
			position:absolute;
			width:100%;
		}


		.gallery-icon img{
			width:70px;
			height:70px;
		}

		.galler-icon{
			float:none;
			display:inline-block;

		}

		.gallery-item {
			margin-top: 10px !important;
			text-align: center !important;
			width: 78px !important;
			float:auto;
			display:inline-block;
		}

		#pGal .entry-title{
			clear:both;
		}

		article h1{
			font-size:20px;
		}

		article img{
			width:auto;
			height:auto;
			width:100px;  float:right;
		}

		.audioGroup{
			clear:both;
		}

		.aGal audio{
			border:1px solid gray;
		}

		#widgetHolder{
			position:absolute;
			top:0px
			left:0px;
			width:0px;
			height:0px;
			z-index:100;
		}

		.widj .eventcontent, .overlay .eventcontent{
		position:relative;
			width: 91%;
			height: 100%;
			top: -5px;
			left:0px;
			padding:10px 10px 0px 10px;
			overflow-y:auto;
			overflow-x:hidden;
			-webkit-overflow-scrolling: touch;
			-moz-border-radius: 52px;
			-webkit-border-radius: 52px;
			 border-radius: 52px;
		}

		#infoLine img {
			width:auto;
			max-width:90%;
			max-height:80%;
		}

		#infoLine {
			text-align:center;
			font-size:1.1em;
			position:fixed; 
			top:0; 
			right:0px;
			max-height:100%;
			z-index:100001;
		}

		#infoline ::-webkit-scrollbar {
			border-radius:10px;
		}

		#infoline ::-webkit-scrollbar-track-piece {
			opacity:0;
		}

		#infoline ::-webkit-scrollbar-thumb {
			opacity:1;
			background-color: black;
			border-radius:10px;
		}

		.eventcontent p {
			background-color:black;
			display:inline;
			padding:3px;
		}

		#infoLine .buttonSimple {
			display:none;
		}

		#cyclers {
			display:none;
		}

		#freakOutMode {
			position:absolute;  
			top:calc(100% - 117px);  
			left:0;
			height:117px;
		}

		#contact {
			position:absolute;  
			top:calc(100% - 200px);  
			height:175px;
			padding:10px 0;
			left:25%;
		}

		.yellow a {
			color:#C7FF1A;
		}

		.button a:hover {
			color:#F00;
			text-decoration:none;
		}

		/* was 480, only screen */
		@media all and (max-width: 768px) and (max-device-width: 768px) {
			.widj {
				z-index: 100;
				width: 100%;
				cursor: move;
				position: relative!important;
				top: auto!important;
				left: auto!important;
				right: auto!important;
				bottom: auto!important;
				height: auto!important;
			}

			#player{
				margin:auto;
				width:100%;
			}

			#spazti-modeSelector, #signIn, #menu{
				display:none!important;
				position:absolute;
			}

			#widgetHolder{
				width:100%;
				position:static;
			}

			body, html{
				overflow:scroll;
			}

			#infoLine {
				max-height:'';
				width:100%;
			}

			/* doesn't work 
			#infoLine span#addy {
				font-size:larger;
			}*/

			#contact {
				width:100%;
			}

			#introSpaz {
				display:none;
			}


		}



	</style>

  </head>
  <body>

  	

  	<div id="introSpaz">
			<h1>SPaz</h1>
			<h2 style="position:relative; top:-100px; -webkit-transform: rotateY( 45deg );-moz-transform: rotateY( 45deg );">mutant dashboard loading...</h2>
	</div>

	<script>
		
	</script>

	<!-- BOOMBOX PLAYER, NEEDS TO BE AT TOP SO IMG LOADS BEFORE ALL THE CYCLES LOAD -->

	<div id="widgetHolder">
		<!-- player -->
		<div id="player" class="widj" style="position:fixed; top:51px;  left:37px;">
		  	<img width="270px" style="z-index:2; position:relative;" src="/images/boombox/spazBox2.gif">
		  	<div id="externalPlayLink">
				<h3>Streaming Now:</h3>
				<div id="spaz_nowplaying"></div>
                <div id="spaz_tip_dj"></div>
				<br>
				<h3>Next DJ:</h3>
				<div id="spaz_next_up_name"></div>
				<div id="spaz_next_up_time"></div>
				<br>
				<h3 class="yellow button" onclick="loadSched();" >
					> Full Radio Schedule
				</h3>
				<h3 class="yellow button"><a id="archivelink" href="radio/archives/" target=_blank>
					> View Archives
					</a></h3>
				<!--h3 class="yellow button">
					<a href="//radio.spaz.org/streams/spazradio.ics">> Full Radio Schedule</a>
				</h3-->
			</div>
			<div id="orListen">
				<a href="//spaz.org/radio.m3u">
					 > External player
				</a>
			</div>

			<div id="jquery_jplayer_1" class="jp-jplayer"></div>
			<div id="jp_container_1" style=" z-index:3;" class="jp-audio-stream">
				<div class="jp-type-single">
				  <div class="jp-gui jp-interface">
					<ul class="jp-controls">
					  <li style="position:absolute;  top:-100px; top: -120px; left: -65px;-webkit-transform: rotate(-3deg);-moz-transform: rotate(-3deg);"><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
					  <li style="position:absolute;  top:-100px; top: -120px; left: -65px;-webkit-transform: rotate(-3deg);-moz-transform: rotate(-3deg);" ><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>

				<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
					  <li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
					  <li style="position: absolute;left: -44px;top: -179px;-webkit-transform: rotate(-7deg);-moz-transform: rotate(-7deg);"><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
					</ul>
					<div class="jp-volume-bar">
					  <div class="jp-volume-bar-value"></div>
					</div>
				  </div>
				  <div class="jp-no-solution">
					<span>External Player</span>
					You don't have HTML5 or Flash.
					<a href="//spaz.org/radio.m3u" target="_self">Use external player instead.</a>
				  </div>
				</div>
			 </div> <!--#jp_container_1 -->
		</div> <!-- #player -->
	</div> <!-- #widgetHolder ... this closing tag was missing -->

	<!-- CYCLING IMAGES -->

	<div id="cyclers">

		<!-- logo starts out w/ hoverspin now.. but make sure it only spins the img not the div!-->
		<div id="logo" class="logo hoverspin">
			<img src="/images/logo/logo_spaz_sunfront_24bit_300x.png" />
		</div>

		<div id="back" class="back">
			<img src="/images/sky/mountains.jpg" />
		</div>

		<div id="dirt" class="dirt">
			<img src="/images/ground/weirdcake.jpg" />
		</div>

		<div id="masks" class="masks">
			<img src="/images/masks/mask_horizon_green_buildings.png"/>
		</div>

	</div> <!-- cyclers -->

	<div id="workModeOverlay">	</div>


	<div id="freakOutMode" class="widj">
		<div class="BGGlass"></div>
		<div class=content>
			<h3 class='yellow button' onclick='setMode(0);'  title="Sponsored by Kevlar"> <span id='mode-0-check'>X </span>Work Mode</h3>
			<h3 class='yellow button' onclick='setMode(1);'  title="Sponsored by Lucas and Serena"><span id='mode-1-check'>X </span >Hung Over Mode</h3>
			<h3 class='yellow button' onclick='setMode(2);'  title="Sponsored by the good people of America"><span id='mode-2-check'>X </span>Good Mode</h3>
			<h3 class='yellow button' onclick='setMode(3);'  title="Sponsored by Terbo Ted"><span id='mode-3-check'>X </span>Seizure Mode</h3>
		</div>
	</div>


	<div id='infoLine' class="widj" title="click to expand">
		<div class="BGGlass"></div>
		<div class="buttonSimple button">X</div>
		<div class="eventcontent">
			<h2>InF0line:</h2>
			(click to expand)<br/>
			
			<a href="/tour">
				
				 
			    <br/>text
				<br/>"spaz" to 
				<br/>888-901-1337
			</a>
		</div>
	</div> <!-- #infoLine -->

	<div id="contact" class="widj">
		<div class="BGGlass"></div>
		<div class="content">
			<center>
				<h2><a href="/radio">Radio Chatroom</a></h2>
              <h2><a href="https://spaz.org/gallery/" target=_blank>Gallery</a></h2>
				<h2><a href="https://www.patreon.com/spazcollective" target=_blank>Spaz Patreon</a></h2>
				
              <h2><a target=_blank href="https://riot.im/app/#/room/#spaz:matrix.org">Riot room</a></h2>
				<h2><a href="https://mail.spaz.org">Web Mail</a></h2>
				<h2><a href="mailto:info@spaz.org">info<span style="font-family:arial">@</span>spaz.org</a></h2>
			</center>
		</div>
	</div>


	<!-- overlays-->
	<div id="overlayBG">
	</div>

	<div id="djSchedule" class="overlay ">
		<div class="BGGlass">
		</div>
		<br>
		<div onclick="$('#djSchedule').fadeOut();$('#overlayBG').fadeOut();" class="buttonSimple button">&times;</div>
		<div class="content djSched">
			<!-- loaded by AJAX at bottom -->
		</div>
	</div>

	<!--div id="eventCalendar" class="overlay">
		<div class="BGGlass">
		</div>
		<h2 onclick="$('#eventCalendar').fadeOut();$('#overlayBG').fadeOut();" class="buttonSimple button">&times;</h2>
		<div class="content">
		</div>
	</div-->

	

  </body>
</html>

<script>

$(document).ready(function(){

	// mobile check
	var winWidth = $(window).width();
	window.isMobile = false;
	if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ||  winWidth <= 576) {
		window.isMobile = true;
	}

	console.log("navigator UA:", navigator.userAgent, "isMobile:",isMobile);

	if(!window.isMobile){

		// only use draggable for non-mobile
		var maxZ=100000;
		$('.widj').draggable({
			start:function(event,ui){
				ui.helper[0].style.zIndex=maxZ;
			},
			stop:function(event,ui){
				winData[ui.helper[0].id].top=parseInt(ui.helper[0].style.top);
				winData[ui.helper[0].id].left=parseInt(ui.helper[0].style.left);
				localStorage.winData1=JSON.stringify(winData);
			},
			containment: "document"
		});

		//infoline enlarge
		$("#infoLine").on("click", function(){
			$(this).css({
				//width:"auto",
				width:"100%",
				height:"100%",
				//top:"1%",
				top:"0",
				//left:"2.5%",
				left:"0",
				cursor:"default",
				opacity:"1",
				fontSize:"1.2em",
			});
			$(this).removeAttr("title");
			$(this).draggable("disable");
			$("#infoLine .buttonSimple").css("display","inline-block");
			$("#infoLine .BGGlass").css({
				background:"url()",
				backgroundColor:"darkblue"
			});
		});

		//return infoline
		$("#infoLine .buttonSimple").on("click", function(event){
			event.stopPropagation();
			$("#infoLine").css({
				width:'',
				height:'',
				top:'0',
				right:'0',
				left:'',
				cursor:"move",
				opacity:"",
				fontSize:"1.1em"
			}).attr("title","click to expand").draggable("enable");
			$(this).css("display","none");
			var gif = $("#contact .BGGlass").css("background");
			$("#infoLine .BGGlass").css({
				background:gif,
				backgroundColor:""
			});
		});
	} // !isMobile

	
}); //docready

window.onload = function() {

	//alert(screen.width);
	// desktop activity
	if(!window.isMobile){

		$('#introSpaz').animate({'opacity':'0'},1000,function(){$('#introSpaz').hide()});
		$('#cyclers').fadeIn(4000);

		jQuery.fx.interval = 100;
		// start the json image gathering 
		// wait til all images are loaded before starting cycles
		$.ajax({
			url: "js/images.json",
			dataType: "json"
		}).done(append_all).then(start_cycles).fail(function( jqXHR, textStatus, errorThrown){ 
			console.log(jqXHR + textStatus + errorThrown);
		});
	}
	
};

function append_images(img){
	console.log("append_images running");
	var images = img["images"];
	//console.log(img["id"]);
	for(var i = 0; i < images.length; i++){
		//console.log(images[i]);
		$('#' + img["id"]).append('<img  src="' + images[i] + '" />');
	}
}

function append_all(images){
	console.log("append_all running");
	//console.log(images);
	for(var i = 0; i < images.length; i++){
		append_images(images[i]);
	}
	console.log("appended all");
}

function start_cycles(){

	console.log("starting cycles");

	$('.logo').cycle({
		fx:     'fadeZoom', 
		pause:  100, 
    	speed:  2009, 
		timeout:  7700,
		random: 1,
		height:"400px",
		containerResize: 0,
		fit: 1 // added to try & fix stretching ish
	});
	$('.back').cycle({
		fx:     'fade', 
		pause:  100, 
    	speed:  5080, 
		timeout:  8000,
		random: 1,
		containerResize: 0,
		fit: 1
	});
	$('.dirt').cycle({
		fx:     'fade', 
		pause:  100, 
    	speed:  4000, 
		timeout:  9057,	
		random: 1,
		containerResize: 0,
		fit: 1
	});
	$('.masks').cycle({
		fx:     'fade', 
		pause:  100, 
		speed:  1000, 
		timeout:  8000,
		random: 1,
		containerResize: 0,
		fit: 1
	});
}


//if (typeof (localStorage.winData1)=='undefined'){
	winData={
		'chat':{
			open:false,
			top:null,
			left:null
		},
		'contact':{
			open:true,
			top:null,
			left:null
		},
		'infoLine':{
			open:true,
			top:null,
			left:null
		},
		'tagWidj':{
			open:false,
			top:null,
			left:null
		},
		'pGal':{
			open:false,
			top:null,
			left:null
		},
		'aGal':{
			open:false,
			top:null,
			left:null
		},
		'essayGal':{
			open:false,
			top:null,
			left:null
		},
		'player':{
			open:true,
			top:null,
			left:null
		},
		'nowPlaying':{
			open:false,
			top:null,
			left:null
		},
		'signIn':{
			open:false,
			top:null,
			left:null
		},
		'freakOutMode':{
			open:true,
			top:null,
			left:null
		},
		'menu':{
			open:false,
			top:null,
			left:null
		}
	}
	localStorage.winData1=JSON.stringify(winData)
	localStorage.spaztiMode=1;
//}



function setMode(mode){
	$('body').removeClass('spazti-mode-0');
	$('body').removeClass('spazti-mode-1');
	$('body').removeClass('spazti-mode-2');
	$('body').removeClass('spazti-mode-3');
	$('body').addClass('spazti-mode-'+mode);
	localStorage.spaztiMode=mode;
	

	// hover spin & display for #logo
	switch(mode) {
		case 0: if($("#logo").css("display") === "block"){
					$("#logo").css("display","none");
				}
				if($("#logo").hasClass("hoverspin")){
					$("#logo").removeClass("hoverspin");
				}
				break;
		case 1: if($("#logo").css("display") === "none"){
					$("#logo").css("display","block");
				}
				break;
		case 2:
		case 3: if($("#logo").css("display") === "none"){
					$("#logo").css("display","block");
				}
				if(!($("#logo").hasClass("hoverspin"))){
					$("#logo").addClass("hoverspin");
				}
				break;
	}
}

$('#pGal,#aGal,#essayGal').resizable({
	handles: "se",
	resize:function(event,ui){
		if (isOverflowed(ui.helper.find('.content')[0])){
			ui.helper.addClass('overflowed');
		}else{
			ui.helper.removeClass('overflowed');
		}
	}
})


function isOverflowed(element){
    return element.scrollHeight > element.clientHeight || element.scrollWidth > element.clientWidth;
}

// all links open in new window so as not to interrupt any user listening to radio
$('a').attr('target', '_blank');
$('.gallery p').remove();




// PROB GOING TO DELETE THIS / REFERS TO EXISTING MENU WHICH I DON'T THINK I WILL USE
function toggleWindow(elName,setVisible,intro){
	if (intro){length=1000; delayMax=5000}else{length=400; delayMax=0}
	winData[elName.substring(1)].open=setVisible
	localStorage.winData1=JSON.stringify(winData)
	if(typeof(setVisible!='undefined')){
		delay=Math.floor(Math.random()*delayMax)
		if (setVisible){
			$(elName).delay( delay ).fadeIn(length);
			$(elName+'Butt').delay( delay ).hide(length);
		}else{
			$(elName).hide(length);
			$(elName+'Butt').show(length);
		}
	}else{
		$(elName).toggle(length)
		$(elName+'Butt').toggle(length);
	}
}


// on load set up dashboard according to localStorage

// set sepazti-mode
setMode(localStorage.spaztiMode)
// position windows
winData=JSON.parse(localStorage.winData1);
for (win in winData) {
	if (winData[win].open){
		toggleWindow("#"+win,true,true)
	}else{
		toggleWindow("#"+win,false)
	}
	if(winData[win].top!=null){
		$('#'+win).css({'top':winData[win].top})
		$('#'+win).css({'left':winData[win].left})
	}
}

</script>
