$(document).ready(function(){
    var port = (location.protocol === 'https:') ? 8060 : 8050;
    var url = "//radio.spaz.org:" + port;
    var stream = {
			title: "SPAZ",
			oga: url + "/radio.ogg",
			mp3: url + "/radio"
    };
    var ready = false;
    window.stream = stream;


    $("#jquery_jplayer_1").jPlayer({
	ready: function (event) {
	    ready = true;
	    $(this).jPlayer("setMedia", stream);
	},
	pause: function() {
	    $(this).jPlayer("clearMedia");
	},
	error: function(event) {
	    if(ready && event.jPlayer.error.type === $.jPlayer.error.URL_NOT_SET) {
		$(this).jPlayer("setMedia", stream).jPlayer("play");
	    }
	    if(event.jPlayer.error.type === $.jPlayer.error.NO_SOLUTION) {
		$("#jquery_jplayer_1").after('<iframe src="//spaz.org/radio.m3u"></iframe>');
		$("#jp_container_1").hide();
	    }
	},
	ended: function(event) {
	    if(ready) {
		$(this).jPlayer("setMedia", stream).jPlayer("play");
	    }
	},
	swfPath: "/js",
	supplied: "oga,mp3",
	solution: "html,flash",
	preload: "none",
	wmode: "window"
    })
});
