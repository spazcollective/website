// Sadly, necessary, via
// https://stackoverflow.com/questions/4071872/html5-video-force-abort-of-buffering
// Otherwise, people players cut out as their network bandwidth gets overwhelmed
// and, worse, dj's on cell connections get their shows cut off due to background dl'ing


function fixAudio(ev, parent){
    var audio = ev.srcElement;

    audio.pause(0);

    var tmp = audio.currentSrc;
    audio.src = "";
    audio.load();
    audio.remove();

    var audio = $("<audio>", {'controls': 'controls',
			      'preload': 'none'})[0];


    // miserable, nasty-ass hack, but it works
    $($(parent).children('br')[0]).after(audio)

    audio.src = tmp;
    audio.addEventListener('pause', function(ev){fixAudio(ev, parent)});

}



$( document ).ready(function() {
    $.map($(".archive-entry"), function(parent){
	var audio = $(parent).children('audio')[0];
	audio.addEventListener('pause', function(ev){fixAudio(ev, parent)})});
});
