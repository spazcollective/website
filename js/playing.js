// NOW PLAYING related functions

var spazradio_where = "//radio.spaz.org/streams/playing-jsonp";


// Constants
var JSONP_CALLBACK = 'update_meta';
var REFRESH_RATE = 10000;


// Global tracks interval ID
var update = null;

// Called to load playing data via JSONP
// Also removes existing JSONP script blocks
function loadData()
{
    var script = null;

    // Check to see if the interval is configured
    // Setup the interval if it is not configured
    if( update == null )
    {
	update = setInterval( loadData, REFRESH_RATE );	
    }

    // Get any of the script blocks with 'callback=' in the source attribute
    script = document.querySelectorAll( 'head script[src*="callback=update_meta"]' );
    
    // Remove any JSONP script blocks that are already in place
    for( var s = 0; s < script.length; s++ )
    {
	script[s].parentNode.removeChild( script[s] );
    }
    
    // Request the playing data via JSONP
    script = document.createElement( 'script' );  
    script.src = spazradio_where + '?callback=' + JSONP_CALLBACK;  
    document.getElementsByTagName( 'head' )[0].appendChild( script );
}



// This JSONP callback gets called every REFRESH_RATE by the above loadData loop
// It's the main entrypoint to updating dynamic DJ-driven data, i.e. current show, download, tip, etc.
function update_meta(m){

    // Now playing
    $("#spaz_nowplaying").html('<span class=yellow>' + m['playing'] + '</span>');

    // Tips
    processTipURL(m['url']);
    
    // Download link
    $("#spaz_download_link").html(m['download'] ? '<a target="_blank" href="https://radio.spaz.org/' + m['download'].replace('/usr/share/airtime/php/legacy/public', '') + '" download>DL This Show</a>' : "");
    
}


$(document).ready(function(){

    // kick it off first
    loadData();

});
