$(function(){
    checkStream();
});

// if spukkin.website ssl certificate is expired, it won't ever detect the stream
// as of Apr 6, 2022 it's good until Thu, 20 Apr 2023 23:59:59 GMT
var videosrc = 'https://livestream.spukkin.website/hls/test.m3u8';

// check if stream exists
function checkStream() {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', videosrc);
  xhr.onload = function() {
    if (xhr.status === 200) {
      // show icon, fade out notification maybe toaster style
      videoAvailable();
    } else {
      // status = 404
      setTimeout(function(){
        checkStream();
      }, 300000); // check every 5 minutes
    } 
    console.log('video stream', xhr.status);        
  };
  xhr.send();
}

function videoAvailable() {
  $('#videotoggle').removeClass('hidden');
  $('#video-toaster').animate({left: 0});
  setTimeout(function(){
    $('#video-toaster').animate({width: 0});
  }, 3000);
  $("#radionav > ul").prepend('<li><a id="radionav-videolink" class="yellow" href="#" onclick="toggleVideo(); return false;">View live video</a></li>');
}

function toggleVideo() {
  var $body = $('body');
  // start
  if (!$body.hasClass('video-streaming')) {
    stopLoadingImages();
    $body.addClass('video-streaming');
    if (!$body.hasClass('visuals-only')) {
      toggleElements();
    }
    var $video = $('<video-js id="cam-feed" class="vjs-default-skin" controls preload="auto" width="1920" height="1080"><source type="application/x-mpegURL"></video-js>');
    $body.prepend($video);
    $('#cam-feed > source').attr('src', videosrc);
    window.videoPlayer = videojs('cam-feed');
    // stop the boombox because video has its own sound
    $("#jquery_jplayer_1").jPlayer("stop");
    $('#radionav-videolink').text('Kill video').toggleClass('red');
    videoPlayer.play();
  } else {
    // stop
    videoPlayer.dispose();
    $body.removeClass('video-streaming');
    // show stuff again so user can click play on boombox
    if ($body.hasClass('visuals-only')) {
      toggleElements();
    }
    $('#videotoggle').removeClass('hidden');
    $("#jquery_jplayer_1").jPlayer("setMedia", stream).jPlayer("play");
    $('#radionav-videolink').text('View live video').toggleClass('red');
    startLoadingImages();
  }     
}


// videoIsLive = videoPlayer.duration() === Infinity;
