function start_cycles(){

	$('.logo').cycle({
		fx:     'fadeZoom', 
		pause:  100, 
    	speed:  2009, 
		timeout:  7700,
		random: 1,
	});
	$('.back').cycle({
		fx:     'fade', 
		pause:  100, 
    	speed:  5080, 
		timeout:  8000,
		random: 1,
	});
	$('.dirt').cycle({
		fx:     'fade', 
		pause:  100, 
    	speed:  4000, 
		timeout:  9057,	
		random: 1,
	});
	$('.masks').cycle({
		fx:     'fade', 
		pause:  100, 
		speed:  1000, 
		timeout:  8000,
		random: 1,
	});
	$('.side-A').cycle({
		fx:     'turnRight', 
		pause:  105, 
    	speed:  1050, 
		timeout:  8500,
		random: 1,
	});
	$('.side-B').cycle({
		fx:     'turnLeft', 
		pause:  130, 
    	speed:  1030, 
		timeout:  4300,
		random: 1,
	});
	$('.side-C').cycle({
		fx:     'turnRight', 
		pause:  140, 
    	speed:  1040, 
		timeout:  2400,
		random: 1,
	});
}


function append_images(img){
	var images = img["images"];
	//console.log(img["id"]);
	for(var i = 0; i < images.length; i++){
		//console.log(images[i]);
		$('#' + img["id"]).append('<img  src="' + images[i] + '" />');
	}
}



function append_all(images){
	//console.log(images);
	for(var i = 0; i < images.length; i++){
		append_images(images[i]);
	}
	start_cycles();
}


$(document).ready(function() {

	// Important! Do not kill people's CPU's!
	jQuery.fx.interval = 100;

	$.ajax( { url: "/js/images.json",
			  success: append_all,
			  //error: function( jqXHR, textStatus, errorThrown){ console.log(jqXHR + textStatus + errorThrown);},
			  dataType: "json"});
});
