$(document).ready(function(){
    var stream = {
	title: "",
	oga: ""
    },
    ready = false;

    $.jPlayer.timeFormat.showHour = true; // shows entire duration instead of just seconds


    // load based on which show = clicked
    $(".show-title").click(function(event){
    	event.preventDefault();
    	stream.oga = $(this).attr("href");
    	$("#jquery_jplayer_1").jPlayer("setMedia", stream).jPlayer("play"); // plays immediately on show title click
    	console.log(stream.oga);
    	stream.title = $(this).text();
    	console.log(stream.title);
    	$("#currentShow").text(stream.title);
    });


    $("#jquery_jplayer_1").jPlayer({
		ready: function (event) {
		    ready = true;
		    $(this).jPlayer("setMedia", stream);
		    if($("#jquery_jplayer_1").attr('autoplay')){
			$(this).jPlayer("setMedia", stream).jPlayer("play");
		    }
		},
		pause: function() {
		    $(this).jPlayer("pause"); //changed from "clearMedia" which obvi does not pause it.
		},
		error: function(event) {
		    if(ready && event.jPlayer.error.type === $.jPlayer.error.URL_NOT_SET) {
			$(this).jPlayer("setMedia", stream).jPlayer("play");
		    }
		    if(event.jPlayer.error.type === $.jPlayer.error.NO_SOLUTION) {
			$("#jquery_jplayer_1").after('<iframe src="//spaz.org/radio.m3u"></iframe>');
			$("#jp_container_1").hide();
		    }
		},
		ended: function(event) {
		    if(ready) {
			$(this).jPlayer("setMedia", stream).jPlayer("play");
		    }
		},
		swfPath: "/js",
		supplied: "oga,mp3",
		solution: "html,flash",
		preload: "none", // changed to "metadata" in attempt to get correct duration, no effect
		wmode: "window",
		//playbar stuff
		smoothPlayBar: true,
		remainingDuration: true,
		toggleDuration: true // switches between time remaining & total time 
    })
});
