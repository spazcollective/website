// TIP RELATED FUNCTIONS 

// this is the default that gets plugged in if there is no show address
var stationTipAddy = "bitcoincash:qphladhssvmgw6ynzqv58fe5alzpscrwlvjhuzvjry",
// var stationTipAddy = "1BDB9B7ZosYU2iBZK4uemVrYF5Hofe6iDw", // legacy
currentTipAddy = false;

function processAddy(addy) {
    // Check which format bch addy is & convert it to legacy so it can be monitored
    var isLegacyAddress = bchaddr.isLegacyAddress,
    toLegacyAddress = bchaddr.toLegacyAddress,
    isCashAddress = bchaddr.isCashAddress,
    toCashAddress = bchaddr.toCashAddress;

    checkAddy(addy);

    // needs to be converted to legacy address if it's in the new BCH form cuz that's what the utx sub data uses
    function checkAddy(address) {
        var format = {};
        // because address checker throws an uncatchable script-halting error if the address is bad
        window.onerror = function(message, error) {
            console.log(message);
            if (address.indexOf("bitcoincash:") > -1) {
                // strip prefix and try again
                address = address.slice(12);
                console.log("might be improperly prefixed legacy address, trying instead:", address);
                checkAddy(address);
            } else {
                console.log("invalid tip address supplied, halting");
                if ($("#spaz_tip_dj").length > 0) {
                  $("#spaz_tip_dj").remove();
                }
                // make sure it only sets it up once
                // if (currentTipAddy !== stationTipAddy) {
                //   return useStationAddy();                    
                // }
            }
            return true;
        }

        if(!isLegacyAddress(address)) {
            // this throws an error if it's an invalid bch address
            format.legacy = toLegacyAddress(address);
            format.cash = toCashAddress(format.legacy);
        } else {
            console.log("already in legacy format");
            format.legacy = address;
            format.cash = toCashAddress(format.legacy);
        }

        console.log("address:", format);
        monitorChain(format.legacy);

        // see if this is station or live show addy 
        var typeOfTip = format.cash === stationTipAddy ? "station" : "DJs";

        // set the QR codes / links in valid URL format with bitcoincash: prefix
        console.log("sending address to QR code:", format.cash);
        setQR(format.cash, typeOfTip);      
    } // end checkAddy   

} // end processAddy



function setQR(addy, type) {
    // make sure this only gets put there once
    if($("#radionav-tiplink").length < 1) {
      makeLink(type);
    }   
    $("#qrcode").html("");
    new QRCode(document.getElementById("qrcode"), addy);
    $("#qrtoggle").attr("title", `tip the ${type} with crypto`).attr("onclick","tipTheDJ();");
    $("#tiplink").attr("href", addy);
}

function makeLink(type) {
    console.log("appending tip link to radionav");
    $("#radionav > ul").append(`<li><a id="radionav-tiplink" class="yellow" href="#" onclick="tipTheDJ(); return false;">Tip the ${type}!</a></li>`);
    $("#snap-a-pic").text(`Tip the ${type} in Bitcoin Cash (BCH)`);
    $("#qrtoggle").append('<img src="../radio/images/BitcoinCash.png">');
    var winWidth = $(window).width();
    // keep hidden by default
    // if (winWidth >= 768) {
    //     $("#qr-wrap").removeClass("hidden");
    // } 
}

// TIP MONITOR WeBSOCKET FUNCTION
function monitorChain(addy2watch) {
    console.log("opening websocket to monitor", addy2watch);
    // set up the websocket
    var wsUri = "wss://ws.blockchain.info/bch/inv",
	websocket = new WebSocket(wsUri);

    websocket.onopen = function(evt) { onOpen(evt) };
    websocket.onmessage = function(evt) { onMessage(evt) };
    websocket.onerror = function(evt) { onError(evt) };

    function onOpen(evt) {
        console.log("started monitoring bch txs");
        websocket.send('{"op":"unconfirmed_sub"}');
    }

    function onMessage(evt) {
        // convert tx to object
        var tx = JSON.parse(evt.data);
        // check every single tx's outs to see if it has an out that matches the show's addy
        for (i=0;i<tx.x.out.length; i++) {
            // console.log(tx.x.out[i].addr);
            if (addy2watch === tx.x.out[i].addr) {
		console.log("Blam! Found tx:", tx);
		gotTipped(tx.x.out[i].value);
            }
        }
    }

    function onError(evt) {
        console.log("something went wrong with the websocket :(");
    }
}


// Call this function once there's a valid tip url provided by the DJ
// Pass in the url string
function popup_tip(url) {
    if (url !== stationTipAddy) {
        console.log("DJ has tip info!", url);
    }
    processAddy(url);
}

// called in playing.js when no show on
function useStationAddy() {
    currentTipAddy = stationTipAddy;
    console.log("putting up the station tip address", stationTipAddy);
    popup_tip(stationTipAddy);
    $("#spaz_tip_dj").html("");
}


function processTipURL(url){
    // !currentTipAddy only true very 1st time someone loads the page, so this is run 1st time no matter what
    // Otherwise it's either 1. the station addy (proceed to else if below)
    // 2. a new show started, so launch the live tip addy
    if (!currentTipAddy || (url && currentTipAddy !== url)) {
    // if (true) { // test
        // If there's a show and they have entered an address, put it in
        if(url){
            window.currentTipAddy = url;
            // DJ has entered a URL. Show it.
            $("#spaz_tip_dj").html('<a target="_blank" href="' + url + '">TIP THE DJ</a>');
            popup_tip(url);
        } else {
            // 1st page load and no live show broadcasting
            // put in the default station tip address
            console.log("page loaded & no live show on");
            useStationAddy(); 
        }
    // this is for when a show has ended and now nothing is playing, so need to switch ended show's tip addy to station addy
    } else if (currentTipAddy && !url && currentTipAddy !== stationTipAddy) {
        console.log("live show's over");
        useStationAddy();
    } else {
        // Do nothing! This is the condition for when the station address is already on screen and nothing is changing
        // simply here for reference
    }

}
