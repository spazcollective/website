function intlSupported(){
    // *sigh* fucking firefox doesn't do this.
    return window.Intl && typeof window.Intl === "object";
}

function localTimeZoneOffset(){
    return -(new Date().getTimezoneOffset() / 60);
}

function formatShowTime(startTS, endTS){
    return formatAMPM(parseInt(startTS)) + " - " + formatAMPM(parseInt(endTS));
}


function formatDay(wds){
    var date = new Date();
    var currentDay = date.getDay();
    var distance = parseInt(wds) - currentDay;
    date.setDate(date.getDate() + distance);
    return intlSupported() ?  date.toLocaleString(navigator.language || navigator.userLanguage, {weekday: 'long'}) : date.toLocaleFormat("%A");
}


function formatHour(hs){
    if(intlSupported()){
	var d = (new Date());
	d.setHours(parseInt(hs));
	return d.toLocaleTimeString(navigator.language || navigator.userLanguage, {hour: '2-digit'});
    } else {
	// just, fuck it. 
	return  hs;
    }
}


function loadSched(){

    $.ajax({
        'url': "//radio.spaz.org/djdash/sched/" + localTimeZoneOffset(),
	'contentType': "application/json",
	'dataType': 'jsonp',
	'crossDomain': true,
	'type': 'GET',
        'success': function (r) {
	    try {
		$('#djSchedule .content').html(r.content);
		$(".show-time").each(function(x){$(this).html(formatShowTime($(this).attr('start'), $(this).attr('end')))});	
		$(".weekday").each(function(x){$(this).html(formatDay($(this).attr('weekday')))});
		$(".master-program-hour").each(function(x){$(this).html(formatHour($(this).attr('hour')))});
		$('#djSchedule').fadeIn();
		$('#overlayBG').fadeIn();
		$('#djSchedule .content')[0].scrollTop = $('#djSchedule .content')[0].scrollHeight;
	    } catch(err) {
		console.log(err.stack);
	    }
	},
	'error': function(r){
	    console.log(r);
	}
    });
}

