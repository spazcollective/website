// SCHEDULE related functions

var spazschedule_where = "//radio.spaz.org/streams/up-next";


// Constants
var SCHEDULE_JSONP_CALLBACK = 'update_schedule_meta';
var SCHEDULE_REFRESH_RATE = 120000;

function formatAMPM(ts) {
    // via https://stackoverflow.com/questions/8888491/how-do-you-display-javascript-datetime-in-12-hour-am-pm-format
    var date = new Date(ts);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes +  ampm;
    return strTime;
}

// Global tracks interval ID
var schedule_update = null;

// Called to load playing data via JSONP
// Also removes existing JSONP script blocks
function loadScheduleData()
{
    var script = null;

    // Check to see if the interval is configured
    // Setup the interval if it is not configured
    if( schedule_update == null )
    {
	schedule_update = setInterval( loadScheduleData, SCHEDULE_REFRESH_RATE );	
    }

    // Get any of the script blocks with 'callback=' in the source attribute
    script = document.querySelectorAll( 'head script[src*="callback=update_schedule_meta"]' );
    
    // Remove any JSONP script blocks that are already in place
    for( var s = 0; s < script.length; s++ )
    {
	script[s].parentNode.removeChild( script[s] );
    }
    
    // Request the playing data via JSONP
    script = document.createElement( 'script' );  
    script.src = spazschedule_where + '?callback=' + SCHEDULE_JSONP_CALLBACK;  
    document.getElementsByTagName( 'head' )[0].appendChild( script );
}


function update_schedule_meta(ms){
    current = ms["current"];
    next_up = ms["next"];
    $("#spaz_next_up_name").html(next_up['name']);
    $("#spaz_next_up_time").html(formatAMPM(next_up['start_timestamp']) + ' - ' + formatAMPM(next_up['end_timestamp']));
}


$(document).ready(function(){
    // kick it off first
    loadScheduleData();
});
