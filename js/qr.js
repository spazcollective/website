$(function(){
  // start qr code hidden
  qrHide();
  $("#qr-close").on("click", function() {
    qrHide();
  });
  $("#navtoggle").on("click", function() {
    $("#radionav").toggleClass("visible");
  });

}); // docready

var tipAudio = new Audio('tipsound.ogg');
tipAudio.volume = 0.3;

function qrHide() {
  $("#qr-wrap").addClass("hidden");
  if ($("#got-tipped").css("display") !== "none") {
    $("#got-tipped").css("display","none");
    $("#snap-a-pic").css("display", "block");
  }
}

function tipTheDJ(tipped) {
  if ($(window).width() < 600) {
    var pos = $(window).scrollTop() + 50 + "px";
    $("#qr-wrap").css("top", pos);
  }

  // make sure if successive tips come thru the window doesn't actually end up hidden!  
  if(!tipped && !$("#qr-wrap").hasClass("hidden")) {
    qrHide();
  } else if (!tipped || (tipped && $("#qr-wrap").hasClass("hidden"))) {
    $("#qr-wrap").toggleClass("hidden");
  } 
}

function gotTipped(amt) {
  tipTheDJ(true);
  var d = new Date(),
      nicenow = d.toLocaleTimeString() + " " + d.toLocaleDateString();

  $("#got-tipped").text("DJ just got tipped " + amt + " satoshis in Bitcoin Cash!!! (" + nicenow + ")").css("display","initial");
  $("#snap-a-pic").css("display","none");
  // tipsound
  tipAudio.play();
  // pinball effects
  $("#qr-wrap").effect("bounce", 750, function(){
    $(this).effect("bounce", 1250, function(){
      $(this).effect("shake", 1000, function(){
        $(this).effect("pulsate", 1500, function(){
          $(this).effect("pulsate");
        });
      });
    });
  });
  // 60 second countdown to hide tip message & put back original
  setTimeout(function(){
    $("#got-tipped").hide();
    $("#snap-a-pic").show();
  }, 60000);
}