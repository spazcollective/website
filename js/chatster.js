var chat;
var chatName;

function getScrollbarWidth() {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

    document.body.appendChild(outer);

    var widthNoScroll = outer.offsetWidth;
    // force scrollbars
    outer.style.overflow = "scroll";

    // add innerdiv
    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);        

    var widthWithScroll = inner.offsetWidth;

    // remove divs
    outer.parentNode.removeChild(outer);

    return widthNoScroll - widthWithScroll;
}



function login(){
    chatName=$('#nameInput').val()
    if (chatName){
	// no sense in logging in null/empty
	$('#chatHolder')[0].style.display='block';
	$('#chatHolder input')[0].focus();
	$('#chatName')[0].style.display='none';
	$('#UserIdLabel').html(chatName);
	chat.login(chatName);
    }
}


function send(){
    message=$('#chatInput').val();
    if(message){
	$('#chatInput').val('');
	$('#chatBody')[0].scrollTop = $('#chatBody')[0].scrollHeight;
	chat.sendMessage(message);
    }
}


function rosterChanged(users){
    var uo = $('#usersOnline');
    uo.text("");
    if (users.length < 1 ){
	uo.fadeOut();
    } else {
	$.each(users, function(i){
	    var aaa = $('<span/>')
		.html($("<p>").html(users[i]).text() + '</br>')
		.appendTo(uo);});
	$('#usersOnline').fadeIn();
    }
}

function messageDate(msg){
    var d = 'time_received' in msg ? (new Date(msg.time_received)) :  (new Date());
    return d.toString()
}

function messageReceived(msg){

    if ($('#chatBody')[0].scrollTop + $('#chatBody').height()>=$('#chatBody')[0].scrollHeight-20){
	atBottom=true;
    } else {
	atBottom=false;
    }

    $('#chatBody').append('<span title="' +  messageDate(msg) + '" class="handle">' + $("<p>").html(msg.user).text() + ': </span><span class="message">' + Autolinker.link($("<p>").html(msg.message).text()) + '</span><br />');
    if (atBottom){
	$('#chatBody')[0].scrollTop = $('#chatBody')[0].scrollHeight;
    }

}

function onDisconnect(){
    rosterChanged([]); 
    $('#chatBody').html("");
    $('#chatInput')[0].disabled = true;
    $('#nameInput')[0].disabled = true;
}

function onConnect(){
    $('#chatInput')[0].disabled = false;
    $('#nameInput')[0].disabled = false;
}

function startChat(){
    var params = {'serv': "radio.spaz.org",
		  'history_url': '//radio.spaz.org/djdash/chatlog',
		  'chan': 'spazradio',
		  'onMessage': messageReceived,
                  'onConnected': onConnect,
                  'onDisconnected': onDisconnect,
                  'onRosterChanged': rosterChanged
		 };

    // debug/testing overrides
    //params['history_url'] = '//localhost/djdash/chatlog';

    chat = spaz_radio_chat(params);

}

scrollBarWidth=getScrollbarWidth();



// start the thing
$(document).ready(startChat);



