<meta charset="utf-8">  
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Spatial Penguins Antitheorize Zombies.  So it's like on the infoline.  You gotta call the infoline.  No, no, I don't know the number, but it's gonna be on the infoline">
<html>
<head>
	<title>S.P.A.Z. RADIO</title>
	<link rel="image_src" href="../images/boombox/spazBox2.gif">
	<meta property="og:image" content="../images/boombox/spazBox2.gif?123"/>

	<link rel="stylesheet" href="radioStyle.css">
	<link rel="stylesheet" href="chatster/chatstyle.css">
	<link rel="stylesheet" href="../css/program-schedule.css">
	<link rel="stylesheet" href="style.css">
	<!-- videostreaming player style -->
	<link rel="stylesheet" href="video-js.css">

	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="../js/jquery.jplayer.min.js"></script>
	<script type="text/javascript" src="../js/jquery-ui-effects.min.js"></script>
	<script type="text/javascript" src="../js/player.js"></script>
	<script type="text/javascript" src="../js/qrcode.min.js"></script>
	<script type="text/javascript" src="../js/bchaddrjs-0.2.0.min.js"></script>
	<script type="text/javascript" src="../js/tip.js"></script>
	<script type="text/javascript" src="../js/playing.js"></script>
	<script type="text/javascript" src="../js/schedule.js"></script>
	<script type="text/javascript" src="../js/fullschedule.js"></script>
	
	<script>
		<?php
			$logoURLs=array();
			foreach (glob("../images/logo/*") as $key=>$fileName) {
				$logoURLs[$key]=$fileName;
			}
			$logos=array('urls'=>$logoURLs,'pos'=>0);
			
			$groundURLs=array();
			foreach (glob("../images/ground/*") as $key=>$fileName) {
				$groundURLs[$key]=$fileName;
			}
			$ground=array('urls'=>$groundURLs,'pos'=>1);
			
			$skyURLs=array();
			foreach (glob("../images/sky/*") as $key=>$fileName) {
				$skyURLs[$key]=$fileName;
			}
			$sky=array('urls'=>$skyURLs,'pos'=>1);

			$maskURLs=array();
			foreach (glob("../images/masks/*") as $key=>$fileName) {
				$maskURLs[$key]=$fileName;
			}
			$mask=array('urls'=>$maskURLs,'pos'=>1);
			
			$images=array(
				'logo'=>$logos,
				'ground'=>$ground,
				'sky'=>$sky,
				'mask'=>$mask
			);
			
			echo 'imageURLs='.json_encode($images);
		?>

		var loadImages = true,	// used to actually stop loading images
				pauseLoad = false;


		function nextImage(subArr,el,imClass){
			arr=imageURLs[subArr];
			// making this stoppable, since even when hidden still loads 500mb+ of images
			if (loadImages) {
				if ((typeof(arr.urls[arr.pos])!='undefined')){
					imDiv=document.createElement('div');
					action='nextImage("'+subArr+'","'+el+'","'+imClass+'");'
					imDiv.innerHTML="<img src='"+arr.urls[arr.pos]+"' class='"+imClass+" "+imClass+arr.pos+" ' onload='"+action+"' onerror='"+action+"'>"
					arr.pos++;
					$(el)[0].appendChild($(imDiv).find('.'+imClass)[0]);
				}
			}	
		}	

		function stopLoadingImages() {
			loadImages = false;
		}

		function startLoadingImages() {
			loadImages = true;
			nextImage('logo','#logoImageLoader','logoIm');
			nextImage('ground','#groundImageLoader','groundIm');
		}

	</script>
</head>
<body>
	<img id=bg >
	<div id=logoImageLoader class='webcam-mode-hidden' style="display:block"></div>
	<div id=groundImageLoader class='webcam-mode-hidden'></div>
	<div id=skyImageLoader style="position:absolute; width:0px; left:-200px; display:none;" ></div>
	<div id=maskImageLoader style="position:absolute; width:0px; left:-200px; display:none;"></div>

	<!-- <img id='tribute-photo' class='webcam-mode-hidden' src='../images/joe-angel.jpeg'/> -->





<div id="player" class='visuals-only-hidden'>
	<img src='../images/boombox/spazBox3.gif' id=playerBG>


	<div id="jquery_jplayer_1" class="jp-jplayer"></div>

	  <div id="jp_container_1" class="jp-audio-stream">
		<div class="jp-type-single">
		  <div class="jp-gui jp-interface">
			<div id=playPause>
			  <a href="javascript:;" class="jp-play" tabindex="1">
				<img src="images/controlsPlayLit.png">
			  </a>
			  <a href="javascript:;" class="jp-pause" tabindex="1">
				<img src="images/controlsPauseLit.png">
			  </a>
			</div>	
				<div id=muteHolder>
					<span href="javascript:;" class="jp-mute" tabindex="1" title="mute">X</span>
					<span href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">/</span>
				</div>
				<li style="display:none"><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
				<div class="jp-volume-bar">
			  		<div class="jp-volume-bar-value"><i>&nbsp;VOLUME!</i></div>
				</div>
		  </div>
		  <div class="jp-no-solution">
			<span>External Player</span>
			You don't have HTML5 or Flash. 
			<a href="//spaz.org/radio.m3u" target="_self">Use external player instead.</a>
		  </div>
		</div>
	  </div>
	<div id="playerInfoCurrent">
		Now Playing:
		<span id="spaz_nowplaying"></span>	
	  <span id="spaz_download_link"></span>
    <span id="spaz_tip_dj"></span>
        
		<div id="upcoming"><a href="//radio.spaz.org/streams/spazradio.ics" download>Coming Up</a>: <span id="spaz_next_up_name">Checking...</span> <span id="spaz_next_up_time">Checking...</span></div>
	</div>
  </div>








<!-- <img src='images/chatScreen.png' id=chatBG> -->

<div id="chatWrap" class='visuals-only-hidden'>
	<div id=chatWin >
		<?php include 'chatster/chatUI.php' ?>
	</div>
</div>



	
	
	<div style="position:fixed; bottom:0px;  right:0px; z-index:20" id=external class='visuals-only-hidden'>
		<p> or <a href="//spaz.org/radio.m3u">
		Use an external player...
		</a></p>
	</div>

	<nav id="radionav">
		<!-- external bits -->
		<div id="navtoggle" class="toggle-icon yellow" title="toggle menu">></div>
		<div id="videotoggle" class='toggle-icon hidden' title='toggle live video stream' onclick='toggleVideo();'>
			<img src="../radio/images/video_camera.svg">
			<p id='video-toaster' class='yellow'>Live video stream in progress!</p>
		</div>
		<div id="qrtoggle" class='toggle-icon visuals-only-hidden' title="tip the DJ with crypto"></div>
		<!-- inside the nav -->
		<ul> 
	    <li class="radiohide">
		   	<a class="yellow" onclick="toggleLights();" id="visuals">Turn Visuals Off</a>
	    </li>
	    <li class="radiohide">
	    	<a class="yellow" onclick="toggleElements();" id="visuals-only">Party mode</a>
	    </li>
			<li id="uploadnav" class="radiohide">
		   		<form id="uploadform" class="radiohide" enctype="multipart/form-data" action="gallery/uploader.php" method="POST">
					<input type="hidden" name="MAX_FILE_SIZE" value="8000000">
					<input type="hidden" id="uploaddir" name="dir" value="ground">

					<label class="uploadLabel">
			 			<input id="uploaded_file" name="uploaded_file" type="file" required>
						<span id="upload_status">Add Pic</span>
					</label>
		 		</form>
			</li>
			<li>
				<a class="yellow" href="archives" target="_blank">Archives</a>
	    </li>
	    <li>
	    	<a class="yellow" onclick="loadSched();">Schedule</a>
	    </li>
		</ul>
	</nav>

	<!-- DJ SCHEDULE overlays-->
	<div id="overlayBG">
	</div>

	<!-- QR CODE FOR TIPPING DJS -->
	<div id="qr-wrap" class="hidden visuals-only-hidden">
		<div id="qr-close">X</div>
		<p id="got-tipped"></p>
		<p id="snap-a-pic">Tip the DJs in Bitcoin Cash (BCH)</p>
		<a href="#" id="tiplink" target="_blank">
			<div id="qrcode"></div>
		</a>
	</div>

	<div id="djSchedule" class="overlay ">
		<div class="BGGlass">
		</div>
		<br>
		<div onclick="$('#djSchedule').fadeOut();$('#overlayBG').fadeOut();" class="buttonSimple button">&times;</div>
		<div class="content djSched">
			<!-- loaded by AJAX at bottom -->
		</div>
	</div>
	

	<script>


		window.onload=function(){
			setTimeout('$("#jquery_jplayer_1").jPlayer("play");',10);
			
			startLoadingImages(); 
			
			$('#bg')[0].onload=function(){
				$('#bg').fadeIn(10000);
			}
      if(localStorage.getItem('visuals')!='off'){
			   $('#bg')[0].src='//25.media.tumblr.com/47b94658c2b72fd866dab53ed960e9c0/tumblr_mplz0lQpqT1sygny6o1_500.gif';
	     }
		} // window.onload
		
		curLogo=1;
		logoCycler()
		function logoCycler(){
			transSpeed=Math.floor(Math.random() * 10000);
			$('.logoIm'+curLogo).fadeOut(transSpeed);
			curLogo=Math.floor(Math.random() * imageURLs.logo.pos)+1

			$('.logoIm'+curLogo).animate({
				'width':70+Math.floor(Math.random() * 500)+'%'
			}, transSpeed);
			$('.logoIm'+curLogo).css({'-webkit-animation-duration':Math.floor((Math.random() * 90)/3)+'s'}); 

			$('.logoIm'+curLogo).fadeIn(transSpeed);
			setTimeout (logoCycler,Math.floor(Math.random() * 10000)+500);				
		}

		curGround=1;
		groundCycler()
		function groundCycler(){
			transSpeed=Math.floor(Math.random() * 10000);
			$('.groundIm'+curGround).fadeOut(transSpeed);
			curGround=Math.floor(Math.random() * imageURLs.ground.pos)+1
			/*$('.groundIm'+curGround).animate({
				'width':'8000px',
				'height':'8000px'
			}, transSpeed);*/
			$('.groundIm'+curGround).css({'-webkit-animation-duration':10+Math.floor(Math.random() * 1)+'s'}); 

			$('.groundIm'+curGround).fadeIn(transSpeed);
			setTimeout (groundCycler,Math.floor(Math.random() * 7000)+500);

		}
	 
		function toggleLights(){
			if (!pauseLoad){
	            // pauseload is always off by default
				$('#visuals')[0].innerHTML="Turn On Visuals";
				$('#visuals')[0].style.color='red';
				$('#logoImageLoader').hide();
				$('#groundImageLoader').hide();
	            $('#uploadnav').hide();
				$('#bg').hide();
				pauseLoad=true;
				localStorage.setItem('visuals','off');
			}else{
				$('#visuals')[0].innerHTML="Turn Off Visuals";
				$('#visuals')[0].style.color='lime';
				
				$('#logoImageLoader').show();
				$('#groundImageLoader').show();
	            $('#uploadnav').show();
				$('#bg').show();
				
				pauseLoad=false;
				localStorage.setItem('visuals','on');
			}
		}
		
		
		if (localStorage.getItem('visuals')=='off'){
	        // miserable hack, leveraging the fact that pauseload is always false at page load
	        // this, however, will turn off the visuals, so it works. but i don't like it. not one bit.
			toggleLights();
		}

		function toggleElements() {
			$('body').toggleClass('visuals-only');
			$('#visuals-only').toggleClass('red');
			if ($('body').hasClass('visuals-only')) {
				$("#radionav").removeClass('visible');
				$('#visuals-only').text('Turn stuff on');
			} else {
				$('#visuals-only').text('Party mode');
			}
		}
		
	 $('#uploaded_file').change(function() {
	    $('#uploadform').submit();
	 });


	$("#uploadform").submit(function(e) {
	    $('#upload_status').text("Wait...");
	    var fd = new FormData();    
	    fd.append( 'uploaded_file', $('#uploaded_file')[0].files[0] );
	    fd.append( 'dir', $('#uploaddir' ).val() );

	    $.ajax({
	      url: 'gallery/uploader.php',
	     data: fd,
	     processData: false,
	     contentType: false,
	     type: 'POST',
	     error: function(data){
	         $('#upload_status').text("Error");
	         console.log(data);
	         setTimeout(function(){
	             // Put it back
	             $('#upload_status').text("Add Pic");
	         },10000);
	     },
	     success: function(data){
	         $('#upload_status').text("Done!");
	         // TODO: maybe crow about it in the chat!
	         setTimeout(function(){
	             // Put it back
	             $('#upload_status').text("Add Pic");
	         },10000);
	     }
		});

	    e.preventDefault(); 
	});


	/* Monitor amt of users in #usersOnline to adjust screen space for mobile */ 

	function adjustSpace(newusertop,newchattop) {
		// console.log("newusertop",newusertop, "newchattop",newchattop);
		$("#usersOnline").css("top",newusertop);
		$("#chatWrap").css("top",newchattop);
	}

	$(window).load(function(){

		// Only doing this stuff if screen width <= 600px	
		if ($(window).width() <= 600){
			// setting timeout so current chat users (thus box height) will load b4 mutations are observed (5s too fast, 6 seems to work)
			setTimeout(function(){
				// each user adds 15px to height userlist
				// height with 1 user = 55px
				// add 15px to the top offset of #chatWrap
				// subtract 15px to the top offset of #usersOnline
				// PER USER
				var defaultHeight = 55;
				var userlist = $("#usersOnline"),
				initHeight = userlist.outerHeight(),
				initusertop = parseInt(userlist.css("top")),
				initchattop = parseInt($("#chatWrap").css("top"));

				if (initHeight > 55) {
					var diff = initHeight - defaultHeight;
					usertop = initusertop - diff + "px";
					chattop = initchattop + diff + "px";
					adjustSpace(usertop, chattop);
				}				

				var observer = new MutationObserver(function(mutations){
					setTimeout(function(){
						var newHeight = userlist.outerHeight();
						if(newHeight !== initHeight) {
							if (newHeight < 55) {
								console.log("new height", newHeight);
								adjustSpace("-45px", "290px");
							} else {
								var diff = newHeight - defaultHeight;
								// console.log("diff", diff);
								var usertop = initusertop - diff + "px";
								var chattop = initchattop + diff + "px";
								adjustSpace(usertop, chattop);	
							}									
						}
					}, 500);					
				});

				var options = {
					childList:true
				}

				observer.observe(userlist[0],options);

			},6000); // initial delay
		} // screen width check

	}); // windowload

	</script>

	<!-- qr code -->
	<script src='../js/qr.js'></script>
	<!-- video streaming -->
	<script src="js/vendor/video.js"></script>
	<script src="js/vendor/videojs-http-streaming.js"></script>
	<script src="../js/videostreaming.js"></script>

</body>
</html>
