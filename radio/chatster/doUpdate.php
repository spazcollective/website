<?php
ob_start();
$theMsg=htmlspecialchars(trim($_GET['msg']));
$theUser=htmlspecialchars(trim($_GET['user']));
$userId=$_GET['chatId'];
$lineNo=$_GET['lineNo'];
$errors = "";
$callback=$_GET['callback'];
$userAwayTimeoutSecs = 30;

/*------  Update Active Users  -------*/

$cleaned_user = sqlite_escape_string($theUser);
$usersText = "";

//TODO: use mysql instead of this sqlite hack

if ($db = new SQLite3('chat.db')) {

	// CRUCIAL to avoid lockups
	$db->busyTimeout(60000);

	// TODO: use prepare statements instead of this sprintf crap

	if($theUser){
		$stmt = $db->prepare('insert or replace into users values (:user, CURRENT_TIMESTAMP)');
		$stmt->bindValue(':user', $theUser , SQLITE3_TEXT);
		$stmt->execute();
	}
	$stmt = $db->prepare("SELECT username FROM users where (strftime('%s', CURRENT_TIMESTAMP) - strftime('%s', last_seen)) < :timeout order by username");
	$stmt->bindValue(':timeout', $userAwayTimeoutSecs , SQLITE3_INTEGER); 
	$qa = $stmt->execute();
	$users = array();
	if($qa){
		while($res = $qa->fetchArray(SQLITE3_ASSOC)){ 
			array_push($users, $res['username']);
		}
	}
	$usersText = implode("<br>", $users);
	

/*------  Update Log  ------*/
	
	if($theMsg != ""){
		$stmt = $db->prepare('insert into messages (username, message, time_received) values (:user, :msg,  CURRENT_TIMESTAMP)');
		$stmt->bindValue(':user', $theUser , SQLITE3_TEXT);
		$stmt->bindValue(':msg', $theMsg, SQLITE3_TEXT);
		$stmt->execute();
	}

/* ------  Make Chat Output  ------*/ 
	$lines = "";
	$stmt = $db->prepare('SELECT username, message, strftime("%s", time_received) as stamp FROM messages where  cast(strftime("%s", time_received) as integer)  > :lineNo order by time_received');
	$stmt->bindValue(':lineNo',  $lineNo , SQLITE3_INTEGER);
	$qa = $stmt->execute();

	$count = $lineNo; // start with a sane default in case there are no messages.
	while($res = $qa->fetchArray(SQLITE3_ASSOC)){ 
		$lines .= sprintf("<br>\n<span class='handle'>%s:</span> <span class='message'>%s</span>",
				  $res['username'], $res['message']);
		$count = $res['stamp'];
	}
} else {
	trigger_error("Could not open DB", E_USER_ERROR);

}


$lines = sprintf("<br>\n<span class='handle'>%s:</span> <span class='message'>%s</span>",
				  "system", "This is the old chat. Shift-reload your browser to get the new one.");
$usersText = "Shift-reload please";

$output=Array('lines' => $lines,
	      'count' => $count,
	      'users' => $usersText,
	      'errors' =>  $errors . htmlspecialchars(ob_get_contents()));



ob_end_clean();
$encoded =  json_encode($output);
if($callback != ""){
	echo "/**/" . $callback . "(" . $encoded . ")";
} else{
	echo $encoded;
}





?>
