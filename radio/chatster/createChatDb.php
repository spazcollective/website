<?php
// DO THIS ONCE WHEN CREATING NEW DB
if ($db = new SQLite3('chat.db')) {

	$db->exec('CREATE TABLE users (username text primary key, last_seen datetime)');
	$db->exec('CREATE TABLE messages (message text, username text, id integer primary key autoincrement, time_received datetime)');
	$db->exec('CREATE INDEX username_idx ON users (username)');

	$db->exec('CREATE INDEX last_seen_idx ON users (last_seen)');

	$db->exec('CREATE INDEX time_received_idx ON messages (time_received)');

	$db->exec('PRAGMA journal_mode=WAL');
}
?>