<?php 

// error_reporting(-1);

include dirname(dirname(__DIR__)) . '/simple_html_dom.php';

$archiveURL = 'https://spaz.org/archives/archives.rss';

$archivesRaw = file_get_contents(dirname(dirname(__DIR__)) .  '/archives/archives.rss');
// $archivesRaw = file_get_contents('https://spaz.org/archives/archives.rss');

try {
    $archives = new SimpleXMLElement($archivesRaw);
} catch (Exception $e) {
    trigger_error('Error: "' .  $e->getMessage(),  E_USER_ERROR);
}

unset($archivesRaw);

// max amount of archives to show [for page load speed]
$max = 15000;

// <dc:creator> is what contains the show title 

if($archives) {
    $list = array();
    $showTitles = array();
    $i = 0;
    foreach ($archives->channel->item as $entry){
	if($i >= $max){ 
	    break;
	} else {
	    array_push($list,$entry);
	}
        $i++;
    }
    unset($entry);

    // ADDING DYNAMIC ISH TO DROP DOWN ! ! 
    //$var_start = microtime(true);

    // individual name variations
    $spukkin = "spukkin|faceship";
    $damfree = "dam free|damn free";
    $subcult = "subcult|cultura|subrepticia|cult sub|cult rept";
    $flowershop = "5lowershop|5shp";
    $beatmeet = "beat meet|beat_meet";
    $stylus = "stylus|d-syn";

    $variations = array("darkat", $spukkin, $damfree,"stones","unwanted","esoundc", $subcult, $flowershop, $beatmeet, $stylus, "oaklajara", "permafrost", "arbol");

    $notpushed = 0;
    $pushed = 0;

    foreach ($list as $show){
	$name = $show->children('dc',true)->creator;
	foreach ($name as $n) {
	    //$n = $n->plaintext; // only useful for DOM
	    $n = htmlspecialchars($n); 
	    if(!in_array($n,$showTitles, true)){
		$lowern = strtolower($n);
		foreach ($variations as $v){
		    $pipe = strpos($v,"|");
		    if($pipe !== false){
			$exploded = explode("|",$v);
			foreach($exploded as $e){
			    $pos = strpos($lowern,$e);
			    if ($pos !== false) {
				break;
			    }
			}				
		    } else {
			$pos = strpos($lowern,$v);
		    }

		    //exit variation foreach
		    if($pos !== false){
			break;
		    }
		    
		}
		if ($n === '' || $n === 'spaz'){
		    $notpushed++;
		} else if ($pos === false) {
		    array_push($showTitles,$n);
		    $pushed++;
		} else {
		    $notpushed++;
		}		
	    } 
	}
    }
    //echo "shows pushed = " . $pushed . " & not pushed = " . $notpushed . "<br/>";

    //$var_end = microtime(true);
    //echo "variation checking took " . ($var_end - $var_start) . "s to finish<br/>";

} else {
    echo "no result...";
}

?>


<!DOCTYPE HTML>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<html>
    <head>

	<title>SPAZ Radio Archives</title>

	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Spatial Penguins Antitheorize Zombies.  So it's like on the infoline.  You gotta call the infoline.  No, no, I don't know the number, but it's gonna be on the infoline" />
	<meta name="keywords" content="spaz, soundsystem, free, tekno, techno, music, s.p.a.z., " />

	<link href="../../css/textiles.css" rel="stylesheet" type="text/css" />
	<link href="../../skins/green.terbo/jplayer.green.terbo2.css" rel="stylesheet" type="text/css" />
	<script src="https://unpkg.com/wavesurfer.js"></script>
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


	<style>

  	 html {
  	     height:100%;
  	     width:100%;
  	     overflow:hidden;
  	     box-sizing:border-box;
  	 }

  	 body {
  	     background-color: darkblue;
  	     background-image:linear-gradient(darkblue,fuchsia);
  	     text-align:center;
  	     font-size:1.2em;
  	     text-shadow: 0px 0px 14px rgba(0, 0, 255, .5);
  	     height:100%;
  	     width:100%;
  	     overflow:hidden;
  	     margin:0;
  	     
  	 }

  	 nav {
  	     position:absolute;
  	     top:0;
  	     left:3%;
  	 }

  	 nav p {
  	     line-height:23px;
  	 }

  	 nav a:hover {
  	     color:fuchsia;
  	 }

  	 #boombox {
  	     position:fixed;
  	     z-index:0;
  	     width:100%;
  	     height:100%;
  	     background-image:url('/images/boombox/spazBox3.gif');
  	     background-size:cover;
  	     background-repeat:no-repeat;
  	 }

  	 #feed {
  	     box-sizing:border-box;
  	     z-index:10;
  	     position:absolute;
  	     width:50%;
  	     margin:auto;
  	     height:100%;
  	     left:0;
  	     right:0;
  	     max-height:100%;
  	     overflow:auto;
  	     background-color: black;
  	     text-align:center;
  	     border-radius:50px;
  	     box-shadow: inset 0px 0px 100px 14px rgba(0, 0, 0, 1);
	     -moz-box-shadow: inset 0px 0px 100px 14px rgba(0, 0, 0, 1);
	     -webkit-box-shadow: inset 0px 0px 100px 14px rgba(0, 0, 0, 1);
	     box-shadow:0 0 3px 3px chartreuse;
  	 }

  	 .yellow {
  	     text-decoration:none;
  	 }

  	 .blue {
  	     color: darkblue;
  	 }

  	 .archive-entry {
  	     width:66%;
  	     text-align:center;
  	     margin:25px auto 25px auto;
  	     background-color: black;
  	     border-radius:25px;
  	     padding:10px;
  	     box-shadow: 0px 0px 3px 3px chartreuse;
  	 }

	 .show-title {
    	     display:block;
    	     color:fuchsia;
  	     text-decoration:none;
	 }

	 .hidden {
    	     display:none;
	 }

	 audio {
	 }

	 .squiggles {
	     position:relative;
	     float:left;
	 }

	 .duration {
	     left: 0;
	     text-align:center;
	     z-index: 2147483647;
	     width: 100%
	 }


	 #playerHolder {
	     display:none;
	     margin:auto;
	     padding-top:15px;
	     padding-bottom:15px;
	     position:fixed;
	     left:25%;
	     width:48.8%;
	     background-color: black;
	     z-index:15;
	     border-top-left-radius: 50px;
	     border-top-right-radius: 50px;
	     border-bottom-left-radius: 50px;
	     box-shadow: inset 0px 0px 100px 14px rgba(0, 0, 0, 1);
	     -moz-box-shadow: inset 0px 0px 100px 14px rgba(0, 0, 0, 1);
	     -webkit-box-shadow: inset 0px 0px 100px 14px rgba(0, 0, 0, 1);
	     box-shadow:0 4px 3px 0px chartreuse;
	 }

	 @media all and (max-width: 768px) and (max-device-width: 768px) {

    	     html,body{
    		 height:auto;
    		 font-size:1.4em;
    		 overflow:auto;
    		 padding:none;
    	     }

    	     #boombox {
    		 display:none;
    	     }

    	     nav {
    		 position:static;
    		 width:100%;
    	     }

    	     nav p {
    		 line-height:43px;
    	     }

    	     #playerHolder {
		 position:fixed;
		 top:0;
		 left:0;
		 width:100%;
		 font-size:.7em;
	     }

    	     #feed {
    		 position:static;
    		 width:100%;
    		 height:auto;
    		 margin:0;
    	     }

    	     .archive-entry {
    		 width:95%;
    		 padding:5px;
    	     }

    	     audio {
    		 width:100%;
    	     }

	 } /* end media queries */


	</style>

    </head>
    <body>

	<div id="boombox"></div>

	<nav id="archivenav">
	    <p class="yellow">
		<a href="//spaz.org" class="yellow">> HOME </a><br/>
		<a href="//spaz.org/radio" class="yellow">> RADIO </a><br/>
	    </p>
	    
	</nav>

	<!-- FEED OF ARCHIVES -->
	
	<div id="playerHolder"></div>

	<div id="feed">


	    <div id="archiveintro">
		<p id="infotext">
		    Displaying <?php echo $i; ?> recent archives. <br/>
		    <?php 
                    echo '<a href="' . $archiveURL . '" target="_blank" class="yellow">Podcast link (' . $archives->channel->item->count() . ' shows)</a><br/>';
		    ?>		<br/>
		    CLICK SHOW TITLE TO PLAY <br/>
		    RIGHT CLICK > "SAVE LINK AS" TO DOWNLOAD
		</p> 

		<!-- make this dropdown alphabetical.. but rando shows below established / named ones ? -->

		<form name="programsorter">
		    Display by show :
		    <select name="programlist" onChange="filterShows(this.form.programlist);">
			<option value="all" selected>View All</option>
			<option value="darkat">Sound Dimensions w/ DJ Darkat</option>
			<option value="<?php echo $spukkin; ?>">Spukkin Faceship's Fukkin Spaceship</option>
			<option value="<?php echo $damfree; ?>">Dam Free Radio</option>
			<option value="stones">Stones and Bones</option>
			<option value="unwanted">Portland's Most Unwanted</option>
			<option value="esoundc">Ecological Sound Corps</option>
			<option value="<?php echo $subcutl; ?>">Cultura Subrepticia</option>
			<!--option value="garth">Garth Radar</option-->
			<option value="<?php echo $flowershop; ?>">5lowershop</option>
			<option value="<?php echo $beatmeet; ?>">Beat Meet and Greet</option>
			<option value="<?php echo $stylus; ?>">Stylus Flite</option>
			<option value="oaklajara">Oaklajara</option>
			<option value='permafrost'>li'l mc sub-zero permafrost's Unification Field Theory</option>
			<option value='arbol'>SPAZ &amp; Arbol Acoustics</option>
			<!-- insert all the unpredictable shows -->
			<?php
			natsort($showTitles);
			foreach ($showTitles as $st){
			    echo "<option value='".$st."'>".$st."</option>";
			}
			?>
		    </select>
		</form>
	    </div>

	    <?php
	    foreach($list as $entry) {
		$title = $entry->title;
		$audio = $entry->link;
                $duration = $entry->duration;
                // adding image seems to slow everything down more, breaks mobile layout.. testing w/out
                //$image = $entry->image->url;
		$date = $entry->pubDate;
		//cuts extraneous time out
		$link = $audio;

		
		echo '<div class="archive-entry">';
		echo '<a href="' . $link . '" class="show-title">' . $title . '</a>';
		//echo '<img class="squiggles" src="' . $image . '" />';
		echo '<span class="duration">' . $duration . '</span><br/>';
		echo '<span class="pubdate">' . $date . '</span><br/>';
		echo '</div>';
	    }

	    ?>

	</div>

    </body>

    <script>

     function fixDate(text){
	 return (new Date(text)).toLocaleDateString(navigator.language || navigator.userLanguage, 
						    {"weekday": "short", 
						     "year": "numeric", 
						     "day": "numeric", 
						     "month": "short"});
     }

     var showfile, showtitle;

     $(document).ready(function(){
	 // localize date time format/language
	 $(".pubdate").text(function(i, text) { 
	     return fixDate(text);
	 });

	 $(".show-title").click(function(event){
    	     event.preventDefault();
	     $("#playerHolder > a").remove();
    	     $("#playerHolder").css("display","block");
    	     showfile = $(this).attr("href");
	     // plays immediately on show title click
	     // $("#playerHolder").html('Now Playing :<br/><audio controls autoplay><source src="' + showfile + '" type="audio/ogg"></audio><br/>'); 

	     $.getJSON(showfile.replace('imports', 'peaks').replace('.ogg', '.json'))
	      .done(function(peaks){
		  console.log('loaded peaks! sample_rate: ' + peaks.sample_rate);

		  // load peaks into wavesurfer.js
		  wavesurfer.load(showfile, peaks.data);
		  if (typeof wavesurfer.cancelAjax === 'function') {
		      wavesurfer.cancelAjax(); // make it stop abusing people's bandwidth and cpu
		  }
		  wavesurfer.play();
		  showtitle = $(this).text();
		  $("#playerHolder").append('<a href="' + showfile + '" class="yellow currentShow"><span>' + showtitle + '</span></a>');
		  $("#feed").css("paddingTop","105px");
		  $(".currentShow").click(function(event){
		      event.preventDefault();
		  });

	      })
	      .fail(function(jqxdr, textStatus, error){
		  console.log("No peaks, just loading audio, and not ajaxing the whole file in advance " + textStatus + error);
		  wavesurfer.load(showfile);
		  if (typeof wavesurfer.cancelAjax === 'function') {
		      wavesurfer.cancelAjax(); // make it stop abusing people's bandwidth and cpu
		  }
		      wavesurfer.play();
		      showtitle = $(this).text();
		      $("#playerHolder").append('<a href="' + showfile + '" class="yellow currentShow"><span>' + showtitle + '</span></a>');
		      $("#feed").css("paddingTop","105px");
		      $(".currentShow").click(function(event){
			  event.preventDefault();
		      });
	      });
	 }); 

	     var wavesurfer = WaveSurfer.create({
		 container: '#playerHolder',
		 waveColor: 'violet',
		 progressColor: 'purple',
		 backend: 'MediaElement',
		 mediaControls: true,
		 mediaType: "audio"
	     });

	     // wavesurfer.on('ready', function () {
	     //     wavesurfer.play();
	     // });

     }); //docready
	 

	 function filterShows(dropdown) {
	     var show = dropdown.options[dropdown.selectedIndex].value;
	     show = show.toLowerCase();

	     var $archives = $(".archive-entry");
	     var contents,
		 showTitle;

	     if (show.indexOf("|") !== -1) {
		 show = show.split("|");
		 showlen = show.length;
		 $archives.addClass("hidden");
		 for(i=0;i<showlen;i++){
		     $archives.each(function(){
			 contents = $(this).html().toLowerCase();
			 if (contents.indexOf(show[i]) !== -1){
			     $(this).removeClass("hidden");
			 }
		     });
		 }
	     } else {
		 $archives.removeClass("hidden");
		 if (show === "unknown"){
		     $archives.each(function(){
			 showTitle = $(this).find(".show-title").html();
			 if(showTitle.match(/[a-z]/i)) {
			     $(this).addClass("hidden");
			 }
		     });			
		 } else if (show !== "all") {
		     $archives.each(function(){
			 contents = $(this).html().toLowerCase();
			 if (contents.indexOf(show) === -1){
			     $(this).addClass("hidden");
			 }
		     });
		 }
	     } // main else	
	 } // filterShows()

    </script>
