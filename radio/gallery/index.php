<?php  ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ERROR); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<?php include_once('resources/UberGallery.php'); ?>

<head>
    <title>SPAZ Radio Gallery</title>
    
    <link rel="shortcut icon" href="../../images/favicon.png" />

    <link rel="stylesheet" type="text/css" href="css/rebase-min.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="resources/UberGallery.css" />
    <link rel="stylesheet" type="text/css" href="resources/colorbox/1/colorbox.css" />
    
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js"></script>
    <script type="text/javascript" src="resources/colorbox/jquery.colorbox.js"></script>
    <script type="text/javascript" src="deleter.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
	  <?php
		    $validdirs = array('ground', 'sky', 'logo');
		    foreach($validdirs as $dir){
		    echo  "$(\"a[rel='" . $dir . "']\").colorbox({maxWidth: \"90%\", maxHeight: \"90%\", opacity: \".5\"});\n";
	    }
	     ?>
    });
    </script>
    
    <?php if(file_exists('googleAnalytics.inc')) { include('googleAnalytics.inc'); } ?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>

<body>

    <div class="pageWrap">
        
        <h1 id="pageTitle">SPAZ Radio Page Images</h1>

	 <p>These are the images which are rotated, faded, randomized, and mangled as part of the <a href="../">SPAZ Radio Player Page</a>. This is the place where you can contribute to this collaborative interactive online art project.</p>
	 <p>Please note the theme in each section, and keep new images consistent with the theme. i.e. round images with transparent backgrounds for Logos, sky-like images for Sky section, ground-like images for Ground, etc.</p>

        <?php $files = scandir('../../images'); ?>
        <?php foreach ($files as $file): ?>
         <?php if(in_array($file, $validdirs)): ?>
            
            <?php $dir = '../../images/' . $file; ?>
            
            <?php if (is_dir($dir) && $file != '.' && $file != '..'): ?>
                <h2><?php echo ucwords($file); ?></a></h2>

		<form enctype="multipart/form-data" action="uploader.php" method="POST">
	 <input type="hidden" name="MAX_FILE_SIZE" value="8000000">
	 <input type="hidden" name="dir" value="<?= $file ?>">
	 Upload: <input name="uploaded_file" type="file">
	 </form>
                <?php $gallery = UberGallery::init()->createGallery($dir, $file); ?>
			 <?php endif; ?>
            <?php endif; ?>
            
        <?php endforeach; ?>
                
    </div>

</body>

<script type="text/javascript">
$(function() {
$("input[name=uploaded_file]").change(function(){
		$(this).parent().submit()
});
})
</script>
<!-- Page template by, Chris Kankiewicz <http://www.chriskankiewicz.com> -->

</html>
